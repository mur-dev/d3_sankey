<?php

/**
 * @file
 * Bootstrap PHPUnit tests.
 *
 * Since PHPUnit doesn't bootstrap Drupal, and we don't have any way of knowing
 * where the xautoload module is in relation to this module, we have to manually
 * include the files we need.
 */

require_once __DIR__ . '/../../../src/DrupalCoreAdapter.php';
require_once __DIR__ . '/../../../src/PreprocessorInterface.php';
require_once __DIR__ . '/../../../src/Model/Link.php';
require_once __DIR__ . '/../../../src/Model/Node.php';
require_once __DIR__ . '/../../../src/Model/RawSankeyData.php';
require_once __DIR__ . '/../src/Model/KeyedLink.php';
require_once __DIR__ . '/../src/TableGroupingPreprocessor.php';
